# Appium-JS-Example

Example of **Appium UI automated tests** for Android app with **JavaScript** and **Mocha**:


# Goals

Writing Appium mobile UI automated tests for MST app Android app:


# Running the Appium Tests

1. Restore the **Node.js dependencies**: ```npm install```
2. Start the **Android emulator**  or **Real Device**
3. Start **Appium desktop** and bind it to the 127.0.0.1:4724
4. Run the **Mocha tests**: ```npm test```

# Expected Test Results
![Report Result](https://ibb.co/hg0372F)
