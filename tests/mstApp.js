let wd = require("selenium-webdriver"), By = wd.By;
let driver;


beforeEach(async function () {
	this.timeout(30000);
	let appiumCaps = {
		platformName: "Android",
		app: "C:\Users\Galang S\Downloads\mst-qa-0.0.1.apk",
		browserName: '',
		"appium:platformVersion": "12",
		"appium:deviceName": "9cc909ad0612",
		"appium:automationName": "UiAutomator2",
		"appium:appPackage": "com.mstqa",
		"appium:appActivity": ".MainActivity",
	};
	driver = await new wd.Builder().usingServer("http://127.0.0.1:4724/")
		.withCapabilities(appiumCaps).build();

	await driver.manage().setTimeouts({ implicit: 5000 });
	//await driver.manage().timeouts().implicitlyWait(1);
	//await driver.manage().setTimeouts({"type":"implicit", "ms":5000});	
});

afterEach(async function () {
	await driver.quit();
});

const assert = require('assert');

describe('Login and Logout', function () {
	it('Login should be sucess and logout should be sucess', async function () {
		this.timeout(30000);

		let usernameKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Username']"));
		await usernameKeyword.clear();
		await usernameKeyword.sendKeys("mstqa");

		let passwordKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Password']"));
		await passwordKeyword.clear();
		await passwordKeyword.sendKeys("P@ssw0rd");

		let buttonLogin = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Login']]"));
		await buttonLogin.click();



		//  assert.ok(assertHome == "Home");
		await driver.wait(async function () {
			let homeText = await driver.findElement(By.xpath("//android.widget.TextView[@text='Home']"));
			let assertHome = await homeText.getText();
			return assertHome.includes("Home");
		}, 5000);
		console.log('Success Login')

		let buttonLogout = await driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView"));
		await buttonLogout.click();
		console.log('Success Logout')




	});
});

describe('Login and add to do 1', function () {
	it('Login should be sucess and add data success', async function () {
		this.timeout(30000);
		let usernameKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Username']"));
		await usernameKeyword.sendKeys("mstqa");

		let passwordKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Password']"));
		await passwordKeyword.sendKeys("P@ssw0rd");

		let buttonLogin = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Login']]"));
		await buttonLogin.click();

		//  assert.ok(assertHome == "Home");
		await driver.wait(async function () {
			let homeText = await driver.findElement(By.xpath("//android.widget.TextView[@text='Home']"));
			let assertHome = await homeText.getText();
			return assertHome.includes("Home");
		}, 5000);
		console.log('Success Login')

		let buttonToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonToDo.click();

		let inputName = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Name']"));
		await inputName.sendKeys("MST QA Test");

		let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		await inputType.click();

		let typePrimary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Primary']"));
		await typePrimary.click();

		let inputDesc = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Description']"));
		await inputDesc.sendKeys("Testing");


		let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonAddToDo.click();

		await driver.sleep(5000);

		// let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		// let tanggalKecil = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-01"));
		// let tanggalBesar = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-31"));
		// let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
		// let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		// let typePrimary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Primary']"));
		// let typeSecondary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Secondary']"));
		// let typeOther = await driver.findElement(By.xpath("//android.widget.TextView[@text='Other']"));
		// let inputDesc = await driver.findElement(By.xpath("//android.widget.TextView[@text='Input Description']"));

		// let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));



		//android.widget.TextView[@text='']
		//android.widget.EditText[@text='Input Name']
		//android.widget.TextView[@text='']

	});
});

describe('Login and add to do 2 date kecil', function () {
	it('Login should be sucess and add data success', async function () {
		this.timeout(30000);
		let usernameKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Username']"));
		await usernameKeyword.sendKeys("mstqa");

		let passwordKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Password']"));
		await passwordKeyword.sendKeys("P@ssw0rd");

		let buttonLogin = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Login']]"));
		await buttonLogin.click();

		//  assert.ok(assertHome == "Home");
		await driver.wait(async function () {
			let homeText = await driver.findElement(By.xpath("//android.widget.TextView[@text='Home']"));
			let assertHome = await homeText.getText();
			return assertHome.includes("Home");
		}, 5000);
		console.log('Success Login')

		let buttonToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonToDo.click();

		let inputName = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Name']"));
		await inputName.sendKeys("MST QA Test 2");


		let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		await calenderButton.click();

		await driver.sleep(1000)

		let tanggalKecil = await driver.findElement(By.xpath("//android.widget.Button[@content-desc=' Sunday 1 October 2023 ']"));
		await tanggalKecil.click();


		let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
		await btnOKCalender.click();

		await driver.sleep(1000)

		let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		await inputType.click();

		let typeSecondary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Secondary']"));
		await typeSecondary.click();

		let inputDesc = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Description']"));
		await inputDesc.sendKeys("Testing");


		let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonAddToDo.click();

		await driver.sleep(5000);

		// let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		// let tanggalKecil = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-01"));
		// let tanggalBesar = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-31"));
		// let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
		// let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		// let typePrimary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Primary']"));
		// let typeSecondary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Secondary']"));
		// let typeOther = await driver.findElement(By.xpath("//android.widget.TextView[@text='Other']"));
		// let inputDesc = await driver.findElement(By.xpath("//android.widget.TextView[@text='Input Description']"));

		// let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));



		//android.widget.TextView[@text='']
		//android.widget.EditText[@text='Input Name']
		//android.widget.TextView[@text='']

	});
});

describe('Login and add to do 3 date besar', function () {
	it('Login should be sucess and add data success', async function () {
		this.timeout(30000);
		// const image = path.resolve('/storage/emulated/0/Testing/image.jpg')

		let usernameKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Username']"));
		await usernameKeyword.sendKeys("mstqa");

		let passwordKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Password']"));
		await passwordKeyword.sendKeys("P@ssw0rd");

		let buttonLogin = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Login']]"));
		await buttonLogin.click();

		//  assert.ok(assertHome == "Home");
		await driver.wait(async function () {
			let homeText = await driver.findElement(By.xpath("//android.widget.TextView[@text='Home']"));
			let assertHome = await homeText.getText();
			return assertHome.includes("Home");
		}, 5000);
		console.log('Success Login')

		let buttonToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonToDo.click();

		let inputName = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Name']"));
		await inputName.sendKeys("MST QA Test 4");


		let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		await calenderButton.click();

		await driver.sleep(1000)

		let tanggalKecil = await driver.findElement(By.xpath("//android.widget.Button[@content-desc=' Tuesday 31 October 2023 ']"));
		await tanggalKecil.click();


		let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
		await btnOKCalender.click();



		await driver.sleep(1000)

		let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		await inputType.click();

		let typeOther = await driver.findElement(By.xpath("//android.widget.TextView[@text='Other']"));
		await typeOther.click();

		let inputDesc = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Description']"));
		await inputDesc.sendKeys("Testing 4");


		// let chooseFile = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		// await chooseFile.click();

		let el10 = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		await el10.click();
		let el11 = await driver.findElement(By.xpath("//android.widget.TextView[@content-desc='File Manager']"));
		await el11.click();



		let el12 = await driver.findElement(By.xpath("//android.widget.TextView[@text='1Testing']"));
		await el12.click();
		let el13 = await driver.findElement(By.xpath("//android.widget.TextView[@text='image.jpg']"));
		await el13.click();
		let el14 = await driver.findElement(By.xpath("//android.widget.Button[@text='Oke']"));
		await el14.click();


		// await driver.push('')


		let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonAddToDo.click();

		await driver.sleep(5000);

		// let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		// let tanggalKecil = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-01"));
		// let tanggalBesar = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-31"));
		// let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
		// let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		// let typePrimary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Primary']"));
		// let typeSecondary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Secondary']"));
		// let typeOther = await driver.findElement(By.xpath("//android.widget.TextView[@text='Other']"));
		// let inputDesc = await driver.findElement(By.xpath("//android.widget.TextView[@text='Input Description']"));

		// let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));



		//android.widget.TextView[@text='']
		//android.widget.EditText[@text='Input Name']
		//android.widget.TextView[@text='']

	});
});

describe('Login and add to do 4 ddt', function () {
	const data = [
		{
			Name: 'Testing 1',
			Date: '2023-08-01',
			Type: 'Primary',
			Description: 'test 1'
		},
		{
			Name: 'Testing 2',
			Date: '2023-09-01',
			Type: 'Secondary',
			Description: 'test 2'
		},
		{
			Name: 'Testing 3',
			Date: '2023-10-05',
			Type: 'Other',
			Description: 'test 3'
		},
		{
			Name: 'Testing 4',
			Date: '2023-11-17',
			Type: 'Primary',
			Description: 'test 4'
		}
	];

	data.forEach(function(item, i) {
		
	
		const name = item.Name;
		const date = item.Date;
		const type = item.Type;
		const description = item.Description;
		console.log(`Name: ${name}, Date: ${date}, Type: ${type}, Description: ${description}`);

		const date2 = new Date(date);
		const daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

		// Daftar nama bulan dalam bahasa Inggris
		const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

		// Mengambil informasi hari, tanggal, bulan, dan tahun
		const dayOfWeek = daysOfWeek[date2.getDay()];
		const dayOfMonth = date2.getDate();
		const month = months[date2.getMonth()];
		const year = date2.getFullYear();
		it('Login should be sucess and add data with ddt success' + i, async function () {
			this.timeout(30000);
			// const image = path.resolve('/storage/emulated/0/Testing/image.jpg')


			const tgl = `${dayOfWeek} ${dayOfMonth} ${month} ${year}`;

			let usernameKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Username']"));
			await usernameKeyword.sendKeys("mstqa");

			let passwordKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Password']"));
			await passwordKeyword.sendKeys("P@ssw0rd");

			let buttonLogin = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Login']]"));
			await buttonLogin.click();

			//  assert.ok(assertHome == "Home");
			await driver.wait(async function () {
				let homeText = await driver.findElement(By.xpath("//android.widget.TextView[@text='Home']"));
				let assertHome = await homeText.getText();
				return assertHome.includes("Home");
			}, 5000);
			console.log('Success Login')

			let buttonToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
			await buttonToDo.click();



			console.log(tgl)

			let inputName = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Name']"));
			await inputName.sendKeys(`${name}`);

			let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
			await calenderButton.click();

			await driver.sleep(1000)

			let tanggalKecil = await driver.findElement(By.xpath('//android.widget.Button[@content-desc="'+tgl+'"]'));
			await tanggalKecil.click();


			let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
			await btnOKCalender.click();



			await driver.sleep(1000)

			let inputType = await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
			await inputType.click();

			let typeOther = await driver.findElement(By.xpath("//android.widget.TextView[@text=`${type}]`"));
			await typeOther.click();

			let inputDesc = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Description']"));
			await inputDesc.sendKeys("`${description}`");
		});
	});

	it('upload file', async function () {
		// let chooseFile = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		// await chooseFile.click();

		let el10 = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		await el10.click();
		let el11 = await driver.findElement(By.xpath("//android.widget.TextView[@content-desc='File Manager']"));
		await el11.click();



		let el12 = await driver.findElement(By.xpath("//android.widget.TextView[@text='1Testing']"));
		await el12.click();
		let el13 = await driver.findElement(By.xpath("//android.widget.TextView[@text='image.jpg']"));
		await el13.click();
		let el14 = await driver.findElement(By.xpath("//android.widget.Button[@text='Oke']"));
		await el14.click();


		// await driver.push('')


		let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonAddToDo.click();

		await driver.sleep(5000);

		// let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		// let tanggalKecil = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-01"));
		// let tanggalBesar = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-31"));
		// let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
		// let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		// let typePrimary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Primary']"));
		// let typeSecondary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Secondary']"));
		// let typeOther = await driver.findElement(By.xpath("//android.widget.TextView[@text='Other']"));
		// let inputDesc = await driver.findElement(By.xpath("//android.widget.TextView[@text='Input Description']"));

		// let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));



		//android.widget.TextView[@text='']
		//android.widget.EditText[@text='Input Name']
		//android.widget.TextView[@text='']
	});
	
});

describe('Login and add to do 5 view detail and verify', function () {
	it('Login should be sucess and add data success', async function () {
		this.timeout(30000);
		// const image = path.resolve('/storage/emulated/0/Testing/image.jpg')

		let usernameKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Username']"));
		await usernameKeyword.sendKeys("mstqa");

		let passwordKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Password']"));
		await passwordKeyword.sendKeys("P@ssw0rd");

		let buttonLogin = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Login']]"));
		await buttonLogin.click();

		//  assert.ok(assertHome == "Home");
		await driver.wait(async function () {
			let homeText = await driver.findElement(By.xpath("//android.widget.TextView[@text='Home']"));
			let assertHome = await homeText.getText();
			return assertHome.includes("Home");
		}, 5000);
		console.log('Success Login')

		let buttonToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonToDo.click();

		let inputName = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Name']"));
		
		await inputName.sendKeys("MST QA Test 3");

		let vName = await inputName.getText();


		let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		await calenderButton.click();

		await driver.sleep(1000)

		let tanggalKecil = await driver.findElement(By.xpath("//android.widget.Button[@content-desc=' Tuesday 31 October 2023 ']"));
		await tanggalKecil.click();


		let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
		await btnOKCalender.click();



		await driver.sleep(1000)

		let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		await inputType.click();

		let typeOther = await driver.findElement(By.xpath("//android.widget.TextView[@text='Other']"));
		await typeOther.click();

		let inputDesc = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Description']"));
		await inputDesc.sendKeys("Testing 4");


		// let chooseFile = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		// await chooseFile.click();

		let el10 = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		await el10.click();
		let el11 = await driver.findElement(By.xpath("//android.widget.TextView[@content-desc='File Manager']"));
		await el11.click();



		let el12 = await driver.findElement(By.xpath("//android.widget.TextView[@text='1Testing']"));
		await el12.click();
		let el13 = await driver.findElement(By.xpath("//android.widget.TextView[@text='image.jpg']"));
		await el13.click();
		let el14 = await driver.findElement(By.xpath("//android.widget.Button[@text='Oke']"));
		await el14.click();


		// await driver.push('')


		let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonAddToDo.click();


		let viewDetail = await driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"));
		await viewDetail.click();

		let vDetailName = await driver.findElement(By.xpath(
			"/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[2]"));
		let detailName = await vDetailName.getText();
		assert.ok(detailName == "Testihg");
		

		await driver.sleep(5000);



		// let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		// let tanggalKecil = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-01"));
		// let tanggalBesar = await driver.findElement(By.id("native.calendar.SELECT_DATE_SLOT-2023-10-31"));
		// let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
		// let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		// let typePrimary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Primary']"));
		// let typeSecondary = await driver.findElement(By.xpath("//android.widget.TextView[@text='Secondary']"));
		// let typeOther = await driver.findElement(By.xpath("//android.widget.TextView[@text='Other']"));
		// let inputDesc = await driver.findElement(By.xpath("//android.widget.TextView[@text='Input Description']"));

		// let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));



		//android.widget.TextView[@text='']
		//android.widget.EditText[@text='Input Name']
		//android.widget.TextView[@text='']

	});
});

describe('Login and add to do 6 view detail', function () {
	it('Login should be sucess and add data success', async function () {
		this.timeout(30000);
		// const image = path.resolve('/storage/emulated/0/Testing/image.jpg')

		let usernameKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Username']"));
		await usernameKeyword.sendKeys("mstqa");

		let passwordKeyword = await driver.findElement(By.xpath("//android.widget.EditText[@text='Password']"));
		await passwordKeyword.sendKeys("P@ssw0rd");

		let buttonLogin = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Login']]"));
		await buttonLogin.click();

		//  assert.ok(assertHome == "Home");
		await driver.wait(async function () {
			let homeText = await driver.findElement(By.xpath("//android.widget.TextView[@text='Home']"));
			let assertHome = await homeText.getText();
			return assertHome.includes("Home");
		}, 5000);
		console.log('Success Login')

		let buttonToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonToDo.click();

		let inputName = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Name']"));
		await inputName.sendKeys("MST QA Test 4");


		let calenderButton = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		await calenderButton.click();

		await driver.sleep(1000)

		let tanggalKecil = await driver.findElement(By.xpath("//android.widget.Button[@content-desc=' Tuesday 31 October 2023 ']"));
		await tanggalKecil.click();


		let btnOKCalender = await driver.findElement(By.xpath("//android.widget.TextView[@text='OK']"));
		await btnOKCalender.click();



		await driver.sleep(1000)

		let inputType =await driver.findElement(By.xpath("//android.widget.Button[@content-desc='Choose Type']"));
		await inputType.click();

		let typeOther = await driver.findElement(By.xpath("//android.widget.TextView[@text='Other']"));
		await typeOther.click();

		let inputDesc = await driver.findElement(By.xpath("//android.widget.EditText[@text='Input Description']"));
		await inputDesc.sendKeys("Testing 4");


		// let chooseFile = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		// await chooseFile.click();

		let el10 = await driver.findElement(By.xpath("//android.widget.TextView[@text='']"));
		await el10.click();
		let el11 = await driver.findElement(By.xpath("//android.widget.TextView[@content-desc='File Manager']"));
		await el11.click();



		let el12 = await driver.findElement(By.xpath("//android.widget.TextView[@text='1Testing']"));
		await el12.click();
		let el13 = await driver.findElement(By.xpath("//android.widget.TextView[@text='image.jpg']"));
		await el13.click();
		let el14 = await driver.findElement(By.xpath("//android.widget.Button[@text='Oke']"));
		await el14.click();


		// await driver.push('')


		let buttonAddToDo = await driver.findElement(By.xpath("//android.widget.Button[.//android.widget.TextView[@text='Add Todo']]"));
		await buttonAddToDo.click();


		let viewDetail = await driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup"));
		await viewDetail.click();

		
		

		await driver.sleep(5000);




	});
});